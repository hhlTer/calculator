package com.test.calcuator;

import com.test.calcuator.service.calculating.PolishCalculating;
import com.test.calcuator.service.exception.ExpressionFormatException;
import com.test.calcuator.service.DecimalExpression;
import com.test.calcuator.service.validate.ExpressionValidator;
import com.test.calcuator.service.FormatExpression;
import com.test.calcuator.service.LatinExpression;

import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Scanner s = new Scanner(System.in);
        String expression;

        System.out.println("Enter expression. '0': Exit");
        while (!(expression = s.next()).equals("0")){
            try {
                FormatExpression fe = determineExpression(expression);
                calc(fe.getDecimalTemplate(expression));
            } catch (ExpressionFormatException e) {
                System.out.println("Wrong format expression");
            }
        }

    }

    private static FormatExpression determineExpression(String expression) {
        try {
            ExpressionValidator.validateRequestAndThrowExceptionIfExistWrongOperators(expression, LatinExpression.DIGITS);
            return new LatinExpression();
        } catch (ExpressionFormatException e){
            return new DecimalExpression();
        }

    }

    private static void calc(String s) throws ExpressionFormatException {
        PolishCalculating polishCalculating = new PolishCalculating(s);
        System.out.println("result = " + polishCalculating.getDoubleValue());
    }

}
