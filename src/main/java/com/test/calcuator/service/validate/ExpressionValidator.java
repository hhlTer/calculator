package com.test.calcuator.service.validate;

import com.test.calcuator.service.FormatExpression;
import com.test.calcuator.service.exception.ExpressionFormatException;

import java.util.regex.Pattern;

public class ExpressionValidator {

    private final static Pattern OPERATORS = Pattern.compile("[*+\\-/]+");

    public static void validateRequestAndThrowExceptionIfExistWrongOperators(String generalRequest, Pattern digits) throws ExpressionFormatException {
        if (generalRequest.startsWith(OPERATORS.pattern()) && OPERATORS.matcher(generalRequest.substring(1,1)).matches()){
            throw new ExpressionFormatException();
        }

        String toRest = generalRequest.replaceAll(" ", "").replaceAll(digits.pattern(), ";");
        String[] test = toRest.split(";");
        for (String t:
                test) {
            if (t.equals("")){
                continue;
            }
            if (!OPERATORS.matcher(t).matches()){
                throw new ExpressionFormatException();
            }
        }

    }

    public static FormatExpression defineExpression(String expression) {
        return null;
    }
}
