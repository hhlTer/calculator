package com.test.calcuator.service;

import com.test.calcuator.service.exception.ExpressionFormatException;
import com.test.calcuator.service.validate.ExpressionValidator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class LatinExpression implements FormatExpression{

    public static final Pattern DIGITS = Pattern.compile("[IVXML]+");
    public static final Pattern OPERATORS = Pattern.compile("[*\\-/+]+");

    public String getDecimalTemplate(String expression) throws ExpressionFormatException {
        ExpressionValidator.validateRequestAndThrowExceptionIfExistWrongOperators(expression, DIGITS);//throws exception if expression is wrong
        String decimalExplression = formatExpressionToDecimal(expression);

        return decimalExplression;
    }


    @Override
    public boolean isInstanceOfExpression(String expression) {
        try {
            ExpressionValidator.validateRequestAndThrowExceptionIfExistWrongOperators(expression, DIGITS);
        } catch (ExpressionFormatException e){
            return false;
        }
        return true;
    }

    private String formatExpressionToDecimal(String expression) {
        String[] latinLine = Arrays.stream(expression.split(OPERATORS.pattern())).filter(s -> !s.isEmpty()).toArray(String[]::new);
        String[] operatorsLine = Arrays.stream(expression.split(DIGITS.pattern())).filter(s -> !s.isEmpty()).toArray(String[]::new);

        String[] digitsLine = formatLatinToDecimal(latinLine);

        StringBuilder sb = new StringBuilder();
        List<String> concatenateList = new ArrayList<>();
        int j = 0;
        for (int i = 0; i < digitsLine.length; i++) {
            if (digitsLine.length - 1 >= i) {
                concatenateList.add(digitsLine[i]);
                sb.append(digitsLine[i]);
            }

            if (operatorsLine.length - 1 >= i) {
                concatenateList.add(operatorsLine[i]);
                sb.append(operatorsLine[i]);
            }
        }
            return sb.toString();

    }

    private String[] formatLatinToDecimal(String[] latinLine) {
        String[] d = new String[latinLine.length];
        int i = 0;
        for (String l:
             latinLine) {
            d[i++] = toDecimal(l);
        }
        return d;
    }

    private String toDecimal(String l) {
        int result =0;
        int past = 0;
        int size = l.length();
        for (int i = size; i > 0; i--) {
//            if (l.equals("XIV")) {
//                String t = l.substring(2,1);
//            }
//            String test = l.substring(i-1,1);
            int current = formatOne(l.substring(i-1,i));
            if (current < past){
                past = current;
                current *= -1;
            } else {
                past = current;
            }
            result += current;
        }
        return String.valueOf(result);
    }

    private int formatOne(String substring) {
        switch (substring){
            case "I":
                return 1;
            case "V":
                return 5;
            case "X":
                return 10;
            case "M":
                return 50;
            case "L":
                return 100;
                default:
                    return 0;
        }
    }

}
